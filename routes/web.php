<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::resource('customer','CustomerController')->middleware('auth');
Route::resource('bussiness','BussinessController')->middleware('auth');
Route::resource('provider','ProviderController')->middleware('auth');
Route::resource('sale','SaleController')->middleware('auth');
Route::resource('product','ProductController')->middleware('auth');
Route::resource('purchase','PurchaseController')->middleware('auth');
Route::resource('type_user','TypeUserController')->middleware('auth');
Route::resource('services','ServiceController')->middleware('auth');
Route::resource('operator','OperatorController')->middleware('auth');
Route::resource('quantity_session','QuantitySessionController')->middleware('auth');
Route::resource('typeTraetment','TypeTraetmentController')->middleware('auth');
Route::resource('traetment','TraetmentController')->middleware('auth');
Route::resource('sale_traetment','SaleTraetmentController')->middleware('auth');
Route::resource('traetment_process','TraetmentProcessController')->middleware('auth');
Route::resource('comision','CommissionController')->middleware('auth');
Route::resource('payment','PaymentController')->middleware('auth');
Route::resource('expense','ExpenseController')->middleware('auth');
Route::resource('users','UserController')->middleware('auth');

Route::get('event','EventController@index')->middleware('auth');
Route::get('cuadre','BoxController@cuadre_caja')->middleware('auth');
Route::get('listCuadre', 'BoxController@list_cuadre')->middleware('auth');
Route::get('descargar-venta/{id}','SaleController@imprimir');
Route::get('descargar-venta-tratamiento/{id}','SaleTraetmentController@imprimir');
Route::get('descargar-pago-tratamiento/{id}','SaleTraetmentController@pay');
Route::get('descargar-pago-venta/{id}','SaleTraetmentController@pay_sale');

Route::post('eventActividad','EventController@Ajax')->middleware('auth');
Route::resource('box','BoxController')->middleware('auth');

Route::get('cuadre_caja/{id}','BoxController@show_box')->name('mostrar');
