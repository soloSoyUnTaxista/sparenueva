<div class="modal fade" id="createUser" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form method="post" action="{{route('users.store')}}">
      @csrf
          <div class="modal-body">
              <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Nombre de Usuario</label>
                  <input type="text" class="form-control" name="name">
              </div>
              <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Correo Electrónico</label>
                  <input type="email" name="email" class="form-control">
              </div>
              <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">Password</label>
                  <input type="password" name="password" class="form-control">
              </div>  
              <div class="mb-3">
                  <select name="type_user_id" class="form-select" required>
                      <option disabled selected value="">--Tipo de Usuario--</option>
                      @foreach($typeUser as $tu)
                      <option value="{{$tu->id}}">{{$tu->name}}</option>
                      @endforeach
                  </select>
              </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
              <button class="btn btn-primary">Modificar</button>
          </div>
      </form>
    </div>
  </div>
</div>