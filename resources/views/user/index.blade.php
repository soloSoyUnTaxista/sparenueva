@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('user.crear')
    <hr><h2 class="text-center">Usuarios <button data-bs-toggle="modal" data-bs-target="#createUser" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($users))
            <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Correo</th>
                <th>Tipo de Usuario</th>
                <th>Opciones</th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->type_user->name}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#modal-edit-{{$user->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" class="btn btn-warning"><i class="fas fa-edit" aria-hidden="true"></i> </button></a>   
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$user->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>  
                @include('user.edit') 
                @include('user.delete') 
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection