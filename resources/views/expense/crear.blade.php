<div class="modal fade" id="createCustomer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Gastos</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('expense.store')}}" method="post">
            @csrf
          <div class="mb-3">
            <label for="name" class="form-label">Nombre de Gasto</label>
            <input type="text" class="form-control" name="name" required>
          </div>
          <div class="mb-3">
            <label for="quantity" class="form-label">Cantidad</label>
            <input type="text" class="form-control" name="quantity" onkeypress="return validar(event);" required>
          </div>
          <div class="mb-3">
            <label for="description" class="form-label">Descripción</label>
            <textarea name="description" id="" cols="30" rows="2" class="form-control" required></textarea>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Guardar</button>
          </div>
          
        </form>
      </div>
    </div>
  </div>
</div>
