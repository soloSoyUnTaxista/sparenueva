@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('expense.crear')
    <hr><h2 class="text-center">Listado de Gastos<button data-bs-toggle="modal" data-bs-target="#createCustomer" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($expenses))
            <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Descripción</th>
                <th>Opciones</th>
            </tr>
            @foreach($expenses as $expense)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$expense->name}}</td>
                    <td>{{$expense->quantity}}</td>
                    <td>{{$expense->description}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editCustomer-{{$expense->id}}" href="{{action('ExpenseController@edit', $expense->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$expense->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('expense.edit')
                @include('expense.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@push('scripts')
<script>
function validar(evt)
  {
      var code = (evt.which) ? evt.which : evt.keyCode;
      if(code==8){
          return true;
      }else if(code>=46 && code<=57){
          return true;
      }else{
          return false;
      }
  }
</script>
@endpush
@endsection