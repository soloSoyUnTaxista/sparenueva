<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <form action="cuadre">
            <input type="date" name="start" class="form-control" required>
            <input type="date" name="end" class="form-control" required>
            <button class="btn btn-success">Solicitar</button>
        </form>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        @if(isset($fechaActual))
            <a href="{{action('BoxController@imprimir', $fechaActual)}}"><i class="fa fa-print fa-3x fa-lg" style="color: green"></i></a>  
        @endif
    </div>
</div>