@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Listado de Cuadres de Caja Realizados </h2><hr>
    <div class="text-center container">
        <table class="table text-center">
            @if(isset($listCuadre))
                <tr>
                    <th>No.</th>
                    <th>Fecha de Inicio</th>
                    <th>Fecha Final</th>
                    <th>Revisado?</th>
                    <th>Acciones</th>
                </tr>
                @foreach($listCuadre as $listado)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$listado->start}}</td>
                        <td>{{$listado->end}}</td>
                        <td>@if($listado->reviewed == 0)
                                <div class="form-group">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1"> 
                                </div>
                            @elseif($listado->reviewed == 1)
                                <div class="form-group">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" checked> 
                                </div>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('mostrar',$listado->id)}}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                            <a data-bs-toggle="modal" data-bs-target="#modal-reviewed-{{$listado->id}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger btn-sm"><i class="fas fa-check"></i></button></a>
                        </td>
                    </tr>
                    @include('box.confirm_review')
                @endforeach
            @else
                <tr>
                    <td>No Existen Registros</td>
                </tr>
            @endif
        </table>
    </div>
</div>
@endsection