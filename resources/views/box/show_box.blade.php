@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Cierre de Caja </h2><hr>
    
    @include('box.range_date')
    <div class="text-center container">
        <h3>Venta de Productos</h3>
        <table class="table text-center">
            @if(isset($sales))
                <tr>
                    <th>No.</th>
                    <th>Cliente</th>
                    <th>Teléfono</th>
                    <th>Total</th>
                </tr>
                @foreach($sales as $sale)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$sale->customer->fullname}}</td>
                        <td>{{$sale->customer->phone}}</td>
                        <td>Q.{{number_format($sale->total,2)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Total Ventas:</td>
                    <td></td>
                    <td></td>
                    <td>Q. {{number_format($total_sales,2)}}</td>
                </tr>
            @else
                <tr>
                    <td>No Existen Registros</td>
                </tr>
            @endif
        </table>
    </div>
    <div class="text-center container">
        <h3>Venta de Tratamientos</h3>
        <table class="table text-center">
            @if(isset($sale_traetments))
                <tr>
                    <th>No.</th>
                    <th>Cliente</th>
                    <th>Teléfono</th>
                    <th>Pago</th>
                </tr>
                @foreach($sale_traetments as $salet)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$salet->customer->fullname}}</td>
                        <td>{{$salet->customer->phone}}</td>
                        <td>Q.{{number_format($salet->payment,2)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Total: </td>
                    <td></td>
                    <td></td>
                    <td>Q. {{number_format($total_sales_traetments,2)}}</td>
                </tr>
            @else
                <tr>
                    <td>No Existen Registros</td>
                </tr>
            @endif
        </table>
    </div>
    <div class="text-center container">
        <h3>Pagos</h3>
        <table class="table text-center">
            @if(isset($pays))
                <tr>
                    <th>No.</th>
                    <th>Cliente</th>
                    <th>Teléfono</th>
                    <th>Pago</th>
                </tr>
                @foreach($pays as $pay)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$pay->sale_traetment->customer->fullname}}</td>
                        <td>{{$pay->sale_traetment->customer->phone}}</td>
                        <td>Q.{{number_format($pay->payment,2)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Total: </td>
                    <td></td>
                    <td></td>
                    <td>Q. {{number_format($total_pays,2)}}</td>
                </tr>
            @else
                <tr>
                    <td>No Existen Registros</td>
                </tr>
            @endif
        </table>
    </div>
    <div class="text-center container">
        <h3>Gastos</h3>
        <table class="table text-center">
            @if(isset($expenses))
                <tr>
                    <th>No.</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                </tr>
                @foreach($expenses as $expense)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$expense->name}}</td>
                        <td>Q.{{number_format($expense->quantity,2)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td>Total de Gastos: </td>
                    <td></td>
                    <td>Q. {{number_format($total_expenses,2)}}</td>
                </tr>
            @else
                <tr>
                    <td>No Existen Registros</td>
                </tr>
            @endif
        </table>
    </div>
    <div class="text-center container">
        <h3>Comisiones</h3>
        <table class="table text-center">
            @if(isset($traetments_process))
                <tr>
                    <th>No.</th>
                    <th>Cliente</th>
                    <th>Cantidad</th>
                    <th>Fecha</th>
                </tr>
                @foreach($traetments_process as $tp)
                    @if($tp->commission->updated_at->format('Y-m-d') >= $inicio && $tp->commission->updated_at->format('Y-m-d') <= $final)
                        @if($tp->commission->is_payed === 1)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$tp->customer->fullname}}</td>
                                <td>Q.{{number_format($tp->commission->quantity,2)}}</td>
                                <td>{{$tp->updated_at}}</td>
                            </tr>
                        @endif
                    @endif
                @endforeach
                @foreach($ventas as $sale)
                    @if($sale->commission->updated_at->format('Y-m-d') >= $inicio && $sale->commission->updated_at->format('Y-m-d') <= $final)
                        @if($sale->commission->is_payed == 1)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$sale->customer->fullname}}</td>
                                <td>Q.{{number_format($sale->commission->quantity,2)}}</td>
                                <td>{{$sale->updated_at}}</td>
                            </tr>
                        @endif
                    @endif
                @endforeach
                <tr>
                    <td>Total de Comisiones Pagadas: </td>
                    <td></td>
                    <td>Q. {{number_format($total_comisiones,2)}}</td>
                </tr>
            @else
                <tr>
                    <td>No Existen Registros</td>
                </tr>
            @endif
            <tr><td></td><td></td></tr>
            <tr><td></td><td></td><td></td><td></td></tr>
            <tr>
                <td>Total de Ingresos: </td>
                <td></td>
                <td></td>
                <td>Q. {{number_format($total,2)}}</td>
            </tr>
            <tr>
                <td>Total de Egresos: </td>
                <td></td>
                <td></td>
                <td>Q. {{number_format($total_egresos,2)}}</td>
            </tr>
            <tr>
                <td>Dinero en Caja: </td>
                <td></td>
                <td></td>
                <td>Q. {{number_format($caja,2)}}</td>
            </tr>
        </table>
    </div>
    <form action="{{route('box.store')}}" method="post">
        @csrf
        <input type="hidden" name="inicio" value="{{$inicio}}">
        <input type="hidden" name="final" value="{{$final}}">
    </form>
</div>
@endsection