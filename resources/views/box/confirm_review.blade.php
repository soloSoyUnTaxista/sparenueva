<div class="modal fade" id="modal-reviewed-{{$listado->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger" >
                <h5 class="modal-title" id="exampleModalLabel">Confirma la revisión del Cuadre de Caja</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{action('BoxController@destroy',$listado->id)}}" method="POST">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <p>Confirma que desea dar por revisado al cuadre con Fecha de Inicio {{$listado->start}} y Fecha Final {{$listado->end}} </p>
                        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
