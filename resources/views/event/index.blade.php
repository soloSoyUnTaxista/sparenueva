@extends('layouts.admin')
@section('content')
<div class="container">
    <h3>Agenda RENUEVA</h3>
    <div id='calendar'></div>
</div>
@push('scripts')
<script>
    $(document).ready(function () {
   
   var SITEURL = "{{ url('/') }}";
     
   $.ajaxSetup({
       headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
     
   var calendar = $('#calendar').fullCalendar
       ({
           monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
           monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
           dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
           dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
           editable: true,
           buttonText: {
                prev: 'Ant',
                next: 'Sig',
                today: 'Hoy',
                month: 'Mes',
                week: 'Semana',
                day: 'Día',
                list: 'Agenda',
            },
           header:{
               left: 'prev,next today',
               center: 'title',
               right: 'month, agendaWeek, agendaDay'
           },
           events: SITEURL + "/event",
           displayEventTime: false,
           editable: true,
           eventRender: function (event, element, view) {
               if (event.allDay === 'true') {
                       event.allDay = true;
               } else {
                       event.allDay = false;
               }
           },
           selectable: true,
           selectHelper: true,
           select: function (start, end, allDay) {
               var title = prompt('Servicio a Realizar:');
               if (title) {
                   var start = $.fullCalendar.formatDate(start, "Y-MM-DD");
                   var end = $.fullCalendar.formatDate(end, "Y-MM-DD");
                   $.ajax({
                       url: SITEURL + "/eventActividad",
                       data: {
                           title: title,
                           start: start,
                           end: end,
                           type: 'add'
                       },
                       type: "POST",
                       success: function (data) {
                           displayMessage("Servicio Registrado Exitosamente");
   
                           calendar.fullCalendar('renderEvent',
                               {
                                   id: data.id,
                                   title: title,
                                   start: start,
                                   end: end,
                                   allDay: allDay
                               },true);
   
                           calendar.fullCalendar('unselect');
                       }
                   });
               }
           },
           eventDrop: function (event, delta) {
               var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
               var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD");
   
               $.ajax({
                   url: SITEURL + '/eventActividad',
                   data: {
                       title: event.title,
                       start: start,
                       end: end,
                       id: event.id,
                       type: 'update'
                   },
                   type: "POST",
                   success: function (response) {
                       displayMessage("Servicio Actualizado Correctamente");
                   }
               });
           },
           eventClick: function (event) {
               var deleteMsg = confirm("Confirma que desea Eliminar el Servicio?");
               if (deleteMsg) {
                   $.ajax({
                       type: "POST",
                       url: SITEURL + '/eventActividad',
                       data: {
                               id: event.id,
                               type: 'delete'
                       },
                       success: function (response) {
                           calendar.fullCalendar('removeEvents', event.id);
                           displayMessage("Servicio Eliminado");
                       }
                   });
               }
           }
   
       });
   });
    
   function displayMessage(message) {
       toastr.success(message, 'Event');
   } 
     
   
</script>
@endpush
@endsection