<div class="modal fade" id="modal-delete-{{$quantity->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger" >
                <h5 class="modal-title" id="exampleModalLabel">Cantidad de Sesiones</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{action('QuantitySessionController@destroy',$quantity->id)}}" method="POST">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <p>Confirma que desea eliminar la cantidad de sesion {{$quantity->fullname}} </p>
                        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
