<div class="modal fade" id="editQuantity-{{$quantity->id}}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-warning" >
        <h5 class="modal-title" id="exampleModalLabel">Cantidad de Sesiones</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('quantity_session.update',$quantity->id)}}" method="post">
        <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="mb-3">
              <label for="quantity_session" class="form-label">Cantidad de Sesiones</label>
              <input type="number" class="form-control" name="quantity_session" value="{{$quantity->quantity_session}}">
            </div>
            <div class="mb-3">
              <label for="time_frame" class="form-label">Período de Tiempo</label>
              <input type="text" class="form-control" name="time_frame" value="{{$quantity->time_frame}}">
            </div>
            <div class="mb-3">
              <label for="description" class="form-label">Descripción</label>
              <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{$quantity->description}}</textarea>
            </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Editar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
