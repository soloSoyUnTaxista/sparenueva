@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('quantity_session.crear')
    <hr><h2 class="text-center">Cantidad de Sesiones  <button data-bs-toggle="modal" data-bs-target="#createQuantity" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($quantities))
            <tr>
                <th>No.</th>
                <th>C. de Sesiones</th>
                <th>Período de Tiempo</th>
                <th>Descripción</th>
                <th>Opciones</th>
            </tr>
            @foreach($quantities as $quantity)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$quantity->quantity_session}}</td>
                    <td>{{$quantity->time_frame}}</td>
                    <td>{{$quantity->description}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editQuantity-{{$quantity->id}}" href="{{action('QuantitySessionController@edit', $quantity->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$quantity->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('quantity_session.edit')
                @include('quantity_session.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection