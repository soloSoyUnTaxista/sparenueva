@extends('layouts.admin')
@section('content')
<div style="background: white" class="container">
  <hr><h2 class="text-center">Compras</h2><hr>
  <form action="{{route('purchase.store')}}" method="post">
    @csrf
    <div class="row">
      <div class="col-lg-6 col-md-6 col-xs-12">  
          <label for="provider_id" class="form-label">Proveedor</label>
          <select name="provider_id" id="customer_id" class="form-control">
            <option disabled selected>--Elegir una opcion--</option>
            @foreach($providers as $provider)
              <option value="{{$provider->id}}">{{$provider->fullname}}</option>
            @endforeach
          </select>
      </div>
      <div class="col-lg-6 col-md-6 col-xs-12">
        <label for="user_id" class="form-label">Usuario</label>
        <select name="user_id" id="user_id" class="form-control">
          <option value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-xs-12">
        <label for="product_id" class="form-label">Productos</label>
        <select name="product_id" id="product_id" class="form-control">
          <option disabled selected> --Elegir una opcion--</option>
          @foreach($products as $product)
            <option value="{{$product->id}}_{{$product->stock}}_{{$product->price_purchase}}">{{$product->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-lg-2 col-md-2 col-xs-12">
        <label for="price_purchase" class="form-label">P. Venta</label>
        <input type="text" id="price_purchase" class="form-control" readonly>
      </div>
      <div class="col-lg-2 col-md-2 col-xs-12">
        <label for="stock" class="form-label">Stock</label>
        <input type="text" id="stock" class="form-control" readonly>
      </div>
      <div class="col-lg-2 col-md-2 col-xs-12">
        <label for="quantity" class="form-label">Cantidad</label>
        <input type="text" id="quantity" onkeypress="return validar(event)" class="form-control">
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-md-9 col-xs-12">
        <label for="description" class="form-label">Descripción</label>
        <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
      </div>
      <div class="col-lg-3 col-md-3 col-xs-12">
        <a id="addProduct" class="btn btn-success mt-4" ata-bs-toggle="tooltip" data-bs-placement="top" title="Añadir Producto"><i class="fa fa-plus"></i></a>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12" id="table">        
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Producto</th>
            <th scope="col">P. Venta</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Sub-Total</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody id="tblProducts">
          
        </tbody>
        <tfoot>
          <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4 id="total" onkeypress="return formatoNumero(event);">Q. / 0.00</h4><input type="hidden" id="totalCompra" name="total"> </td>
          </tr>
        </tfoot>
</table>
    </div>
    <div class="row">
      <div class="col-lg-10 col-md-10"></div>
      <div class="col-lg-2 col-md-2 col-xs-12">
        <button type="submit" id="guardar" class="btn btn-success text-white mx-rigth">Comprar</button>
      </div>
    </div>
    </form>
</div>
@push('scripts')
<script>
  cont = 0;
  subTotal = [];
  total = 0;
  evaluar();
  function validar(evt)
  {
      var code = (evt.which) ? evt.which : evt.keyCode;
      if(code==8){
          return true;
      }else if(code>=48 && code<=57){
          return true;
      }else{
          return false;
      }
  }
  $('#product_id').change(datos);
  function datos(){
    datos = document.getElementById('product_id').value.split('_');
    stock = datos[1];
    price_purchase = datos[2];
    $('#stock').val(stock);
    $('#price_purchase').val(price_purchase);
  }

  $('#addProduct').click(addProduct)
  function addProduct(){
    datos = document.getElementById('product_id').value.split('_');
    producto = $('#product_id option:selected').text();
    price_purchase = datos[2];
    idProduct = datos[0];

    stock = datos[1];
    stock = parseInt(stock);

    quantity = $('#quantity').val();
    quantity = parseInt(quantity);

    //validando que exista un producto seleccionado
    if(quantity > 0 && price_purchase > 0 && stock != " "){
      //validacion que el stock sea mayor que la cantidad ingresada
      subTotal[cont] = (price_purchase * quantity);
      total = total + subTotal[cont];
      var product = '<tr id="fila'+cont+'">'
                      + '<th scope="col">'+cont+'</th>'
                      + '<input type="hidden" value="'+idProduct+'" name="product_id[]">'
                      + '<th scope="col">'+producto+'</th>'
                      + '<th scope="col">Q.'+price_purchase+'.00 <input type="hidden" name="price_purchase[]" value="'+price_purchase+'"></th>'
                      + '<th scope="col">'+quantity+'<input type="hidden" name="quantity[]" value="'+quantity+'"></th>'
                      + '<th scope="col">Q.'+subTotal[cont]+'.00<input type="hidden" name="subTotal[]" value="'+subTotal[cont]+'"></th>'
                      + '<th scope="col"><button type="button" onclick="eliminar('+cont+');" class="btn btn-danger btn-sm"> X </button></th>'
                    + '</tr>';
      cont++;
      $('#total').html('Q.' + total + '.00');
      $('#totalCompra').val(total);
      $('#tblProducts').append(product);
      evaluar();
      limpiarCampos();
    }
  }
  function limpiarCampos(){
    $('#quantity').val(" ");
    $('#price_purchase').val(" ");
    $('#stock').val(" ");
  }
  function evaluar(){
      if(total>0){
          $('#guardar').show();
          $('#table').show();
      }else {
          $('#guardar').hide();
          $('#table').hide();
      }
  }

  function eliminar(index){
      total = total - subTotal[index];
      $('#total').html('Q. ' + total);
      $('#fila'+index).remove();
      evaluar();
  }

</script>
@endpush
@endsection