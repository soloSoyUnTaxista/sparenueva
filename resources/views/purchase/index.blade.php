@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Compras <a href="{{route('purchase.create')}}" class="btn btn-success" > <i class="fa fa-plus"></i></a></h2><hr>
    <table class="table text-center">
        @if(isset($purchases))
            <tr>
                <th>No.</th>
                <th>Proveedor</th>
                <th>Teléfono</th>
                <th>Total</th>
                <th>Descripción</th>
                <th>Opciones</th>
            </tr>
            @foreach($purchases as $purchase)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$purchase->provider->fullname}}</td>
                    <td>{{$purchase->provider->phone}}</td>
                    <td>Q.{{number_format($purchase->total,2)}}</td>
                    <td>{{$purchase->description}}</td>
                    <td>
                        <a href="{{action('PurchaseController@show', $purchase->id)}}"><button class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i></button></a>   
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$purchase->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('purchase.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection