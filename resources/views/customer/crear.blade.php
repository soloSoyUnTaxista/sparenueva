<div class="modal fade" id="createCustomer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cliente</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('customer.store')}}" method="post">
            @csrf
          <div class="mb-3">
            <label for="fullname" class="form-label">Nombre Completo</label>
            <input type="text" class="form-control" name="fullname" required>
          </div>
          <div class="mb-3">
            <label for="address" class="form-label">Dirección</label>
            <input type="text" class="form-control" name="address">
          </div>
          <div class="mb-3">
            <label for="phone" class="form-label">Teléfono</label>
            <input type="text" class="form-control" name="phone">
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Guardar</button>
          </div>
          
        </form>
      </div>
    </div>
  </div>
</div>
