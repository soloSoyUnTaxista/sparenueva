@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('customer.crear')
    <hr><h2 class="text-center">Listado de Clientes  <button data-bs-toggle="modal" data-bs-target="#createCustomer" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <input type="text" class="form-control" id="searchCustomer" placeholder="Cliente a Buscar...">
    <table class="table text-center" id="tableData">
        @if(isset($customers))
            <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Opciones</th>
            </tr>
            @foreach($customers as $customer)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$customer->fullname}}</td>
                    <td>{{$customer->address}}</td>
                    <td>{{$customer->phone}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editCustomer-{{$customer->id}}" href="{{action('CustomerController@edit', $customer->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$customer->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('customer.edit')
                @include('customer.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#searchCustomer').on("keyup",function(){
            var value = $(this).val().toLowerCase();
            $('#tableData tr').filter(function(){
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
@endpush