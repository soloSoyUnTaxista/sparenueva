<div class="modal fade" id="editTypeTraetment-{{$type->id}}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-warning" >
        <h5 class="modal-title" id="exampleModalLabel">Editar Tipo de Tratamiento</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('typeTraetment.update',$type->id)}}" method="post">
        <input name="_method" type="hidden" value="PATCH">
            @csrf
          <div class="mb-3">
            <label for="name" class="form-label">Nombre</label>
            <input type="text" class="form-control" name="name" value="{{$type->name}}">
          </div>
          <div class="mb-3">
            <label for="description" class="form-label">Descripción</label>
            <input type="text" class="form-control" name="description" value="{{$type->description}}">
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Editar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
