@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('typeTraetment.crear')
    <hr><h2 class="text-center">Tipos de Tratamiento  <button data-bs-toggle="modal" data-bs-target="#createTypeTraetment" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($typeTraetments))
            <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Opciones</th>
            </tr>
            @foreach($typeTraetments as $type)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$type->name}}</td>
                    <td>{{$type->description}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editTypeTraetment-{{$type->id}}" href="{{action('TypeTraetmentController@edit', $type->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$type->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('typeTraetment.edit')
                @include('typeTraetment.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection