<div class="modal fade" id="modal-delete-{{$type->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger" >
                <h5 class="modal-title" id="exampleModalLabel">Tipo de Tratamiento</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{action('TypeTraetmentController@destroy',$type->id)}}" method="POST">
                    @csrf
                    <input name="_method" type="hidden" value="DELETE">
                    <p>Confirma que desea eliminar al Cliente {{$type->name}} </p>
                        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Confirmar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
