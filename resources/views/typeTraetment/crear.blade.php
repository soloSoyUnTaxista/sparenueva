<div class="modal fade" id="createTypeTraetment">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tipo Tratamiento</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('typeTraetment.store')}}" method="post">
            @csrf
          <div class="mb-3">
            <label for="fullname" class="form-label">Nombre</label>
            <input type="text" class="form-control" name="name">
          </div>
          <div class="mb-3">
            <label for="porcentaje" class="form-label">Porcentaje de Comisión (%)</label>
            <input type="integer" class="form-control" name="porcentaje">
          </div>
          <div class="mb-3">
            <label for="address" class="form-label">Descripción</label>
            <input type="text" class="form-control" name="description">
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Guardar</button>
          </div>
          
        </form>
      </div>
    </div>
  </div>
</div>
