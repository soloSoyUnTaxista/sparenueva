@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('service.crear')
    <hr><h2 class="text-center">Servicios  <button data-bs-toggle="modal" data-bs-target="#createService" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($services)) 
            <tr>
                <th>No.</th>
                <th>Servicio</th>
                <th>Precio</th>
                <th>Descripción</th>
                <th>C. de Sesiones</th>
            </tr>
            @foreach($services as $service)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$service->name}}</td>
                    <td>{{$service->price_service}}</td>
                    <td>{{$service->description}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editService-{{$service->id}}" href="{{action('ServiceController@edit', $service->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$service->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('service.edit')
                @include('service.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection