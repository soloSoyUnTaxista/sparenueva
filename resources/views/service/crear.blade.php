<div class="modal fade" id="createService">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Servicios</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('services.store')}}" method="post">
            @csrf
          <div class="mb-3">
            <label for="name" class="form-label">Nombre de Servicio</label>
            <input type="text" class="form-control" name="name">
          </div>
          <div class="mb-3">
            <label for="price_service" class="form-label">Precio</label>
            <input type="number" class="form-control" name="price_service">
          </div>
          <div class="mb-3">
            <label for="quantity_session_id" class="form-label">Período de Tiempo</label>
            <select name="quantity_session_id" id="quantity_session_id" class="form-control">
              <option disabled selected>--Elegir un Periodo--</option>
              @foreach($quantities as $quantity)
                <option value="{{$quantity->id}}">{{$quantity->quantity_session}} - {{$quantity->time_frame}}</option>
              @endforeach
            </select>
          </div>
          <div class="mb-3">
            <label for="description" class="form-label">Descripción</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
