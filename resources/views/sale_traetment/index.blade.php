@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Tratamientos Realizados<a href="{{route('sale_traetment.create')}}" class="btn btn-success" > <i class="fa fa-plus"></i></a></h2><hr>
    <input type="text" class="form-control" id="searchSale" placeholder="Cliente a Buscar">
    <table class="table text-center" id="tableData">
        @if(isset($sales))
            <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Teléfono</th>
                <th>Total</th>
                <th>Abono</th>
                <th>Saldo</th>
                <th>Descripción</th>
                <th>Opciones</th>
            </tr>
            @foreach($sales as $sale)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$sale->customer->fullname}}</td>
                    <td>{{$sale->customer->phone}}</td>
                    <td>Q.{{number_format($sale->total,2)}}</td>
                    <td>Q.{{number_format($sale->payment,2)}}</td>
                    <td>Q.{{number_format($sale->balance,2)}}</td>
                    <td>{{$sale->description}}</td>
                    <td>
                        <a href="{{action('SaleTraetmentController@show', $sale->id)}}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>   
                        @if(Auth::user()->type_user->name == 'admin')
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$sale->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                        @endif
                        <a data-bs-toggle="modal" data-bs-target="#modal-pay-{{$sale->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Pagar" class="btn btn-warning btn-sm"><i class="fas fa-money-bill" aria-hidden="true"></i> </button></a>
                        <a href="{{action('SaleTraetmentController@imprimir', $sale->id)}}"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i></button></a>
                    </td>
                </tr>
                @include('sale_traetment.delete')
                @include('sale_traetment.pay')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@push('scripts')
    <script>
        $(document).ready(function(){
            $('#searchSale').on("keyup",function(){
                var value = $(this).val().toLowerCase();
                $('#tableData tr').filter(function(){
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
        
        function calcularsaldo(id){
            console.log(id);
            saldo = $('#balance-'+id).val();
            saldo = parseInt(saldo);
            pago = $('#payment-'+id).val();
            pago = parseInt(pago);
            
            if(saldo >= pago){
                newsaldo = saldo - pago;
                $('#total-'+id).html('Q. '+newsaldo+ '.00');
                $('#newBalance-'+id).val(newsaldo);
            }else{
                $('#payment-'+id).val(" ");
                alert('El abono excede el saldo pendiente');
            }
            
        }
    </script>
@endpush
@endsection