@extends('layouts.admin')
@section('content')
<div style="background: white" class="container">
  <hr><h2 class="text-center">Ventas</h2><hr>
  <form action="{{route('sale_traetment.store')}}" method="post">
    @csrf
    <div class="row">
      <div class="col-lg-4 col-md-4 col-xs-12">  
          <label for="customer_id" class="form-label">Cliente</label>
          <select name="customer_id" id="customer_id" class="form-control" required>
            <option disabled selected>--Elegir una opcion--</option>
            @foreach($customers as $customer)
              <option value="{{$customer->id}}">{{$customer->fullname}}</option>
            @endforeach
          </select>
      </div>
      <div class="col-lg-4 col-md-4 col-xs-12">
        <label for="user_id" class="form-label">Usuario</label>
        <select name="user_id" id="user_id" class="form-control">
          <option value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
        </select>
      </div>
      <div class="col-lg-4 col-md-4 col-xs-12">
        <label for="operator_id" class="form-label">Vendedor</label>
        <select name="operator_id" id="operator_id" class="form-control" required>
          @foreach($operators as $operator)
            <option value="{{$operator->id}}">{{$operator->fullname}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 col-xs-12">
        <label for="traetment_id" class="form-label">Tratamientos</label>
        <select id="traetment_id" class="form-control" required>
          <option disabled selected> --Elegir una opcion--</option>
          @foreach($typeTraetments as $type)
          <div class="dropdown">
              <option type="button" class="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" disabled>
                {{$type->name}}
              </option>
                <ul class="dropdown-menu">
                  @foreach($traetments as $traetment)
                    @if($type->id == $traetment->type_traetment_id)
                      <li><a class="dropdown-item" href="#"><option value="{{$traetment->id}}_{{$traetment->price_traetment}}_{{$traetment->type_traetment->name}}">{{$traetment->name}}</option></a></li>
                    @endif
                  @endforeach
                </ul>
            </div>
          @endforeach
        </select>
      </div>
      <div class="col-lg-2 col-md-2 col-xs-12">
        <label for="price_traetment" class="form-label">Precio</label>
        <input type="text" id="price_traetment" name="price_traetment" class="form-control" readonly required>
      </div>
      <div class="col-lg-2 col-md-2 col-xs-12">
        <label for="quantity" class="form-label">Cantidad</label>
        <input type="text" id="quantity" name="quantity" onkeypress="return validar(event)" class="form-control" required>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-md-9 col-xs-12">
        <label for="description" class="form-label">Descripción</label>
        <textarea name="description" id="description" cols="30" rows="2" class="form-control"></textarea>
      </div>
      <div class="col-lg-3 col-md-3 col-xs-12">
        <a id="addProduct" class="btn btn-success mt-4" ata-bs-toggle="tooltip" data-bs-placement="top" title="Añadir Producto"><i class="fa fa-plus"></i></a>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12" id="table">        
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Tratamiento</th>
            <th scope="col">Tipo T.</th>
            <th scope="col">Precio</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Sub-Total</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
        <tbody id="tblProducts">
          
        </tbody>
        <tfoot>
          <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4 id="total">Q. / 0.00</h4><input type="hidden" id="totalVenta" name="total"> </td>
            <td><h4 id="descuento">Q. / 0.00</h4><input type="hidden" id="totalDescuento" value="0" name="totalDescuento"></td>
            <td>
              <select name="type_discount" id="type_discount" class="form-control">
                <option value="0">0%</option>
                <option value="0.05">5%</option>
                <option value="0.10">10%</option>
                <option value="0.15">15%</option>
                <option value="0.20">20%</option>
                <option value="0.25">25%</option>
                <option value="0.30">30%</option>
                <option value="0.35">35%</option>
                <option value="0.3333">Combo</option>
              </select>
          </td>
          </tr>
          <tr>
            <td>Abono:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><input onkeypress="return validar(event);" type="text" class="border-bottom" style="font-size: 22px; border: none; width:60%" id="payment" name="payment" required></td>
          </tr>
          <tr>
            <td>Saldo:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4 id="balance"></h4><input type="hidden" id="balanceFinal" name="balance"></td>
          </tr>
        </tfoot>
</table>

    </div>
    <div class="row">
      <div class="col-lg-10 col-md-10"></div>
      <div class="col-lg-2 col-md-2 col-xs-12">
        <button id="guardar" type="submit" class="btn btn-success text-white mx-rigth">Vender</button>
      </div>
    </div>
    </form>
</div>
@push('scripts')
<script>
  cont = 0;
  subTotal = [];
  total = 0;
  totalDescuento = 0;
  evaluar();
  function validar(evt)
  {
      var code = (evt.which) ? evt.which : evt.keyCode;
      if(code==8){
          return true;
      }else if(code>=48 && code<=57){
          return true;
      }else{
          return false;
      }
  }
  $('#traetment_id').change(datos);
  function datos(){
    datos = document.getElementById('traetment_id').value.split('_');
    price_traetment = datos[1];
    $('#price_traetment').val(price_traetment);
    $('#quantity').val('1');
  }

  $('#addProduct').click(addProduct)
  function addProduct(){
    descuento = $('#totalDescuento').val();
    datos = document.getElementById('traetment_id').value.split('_');
    traetment = $('#traetment_id option:selected').text();
    price_traetment = datos[1];
    idTraetment = datos[0];
    typeTraetment = datos[2];

    quantity = $('#quantity').val();
    quantity = parseInt(quantity);

    //validando que exista un producto seleccionado
    if(quantity > 0 && price_traetment > 0){
      //validacion que el stock sea mayor que la cantidad ingresada
      price_traetment = parseInt(price_traetment);
      quantity = parseInt(quantity);
      subTotal[cont] = (price_traetment * quantity);
      total = total + subTotal[cont];
      total = total - descuento;

      var product = '<tr id="fila'+cont+'">'
                      + '<td scope="col">'+cont+'</td>'
                      + '<input type="hidden" value="'+idTraetment+'" name="traetment_id[]">'
                      + '<td scope="col">'+traetment+'</td>'
                      + '<td scope="col">'+typeTraetment+'</td>'
                      + '<td scope="col">Q.'+price_traetment+'.00 <input type="hidden" name="price_traetment[]" value="'+price_traetment+'"></td>'
                      + '<td scope="col">'+quantity+'<input type="hidden" name="quantity[]" value="'+quantity+'"></td>'
                      + '<td scope="col"><h4 id="subtota'+cont+'">Q.'+subTotal[cont]+'.00</h4><input type="hidden" name="subTotal[]" id="subtotal'+cont+'" value="'+subTotal[cont]+'"></td>'
                      + '<td scope="col"><button type="button" onclick="eliminar('+cont+');" class="btn btn-danger btn-sm"> X </button></td>'
                    + '</tr>';
      cont++;
      $('#total').html('Q.' + total + '.00');
      $('#totalVenta').val(total);
      $('#tblProducts').append(product);
      evaluar();
      limpiarCampos();
    }else{
      alert('Ingrese una Cantidad Válida');
    }
  }
  $('#payment').change(saldo);
  function saldo(){
    abono = $('#payment').val();
    console.log(abono);
    total = $('#totalVenta').val();
    saldo = total - abono;
    $('#balance').html(saldo);
    $('#balanceFinal').val(saldo);
  }

  function limpiarCampos(){
    $('#quantity').val(" ");
    $('#price_traetment').val(" ");
  }
  
  function evaluar(){
      if(total>0){
          $('#guardar').show();
          $('#table').show();
      }else {
          $('#guardar').hide();
          $('#table').hide();
      }
  }

  function eliminar(index){
      total = total - subTotal[index];
      $('#total').html('Q. ' + total);
      $('#fila'+index).remove();
      evaluar();
  }

  $('#type_discount').change(discount);
  function discount(){
    descuento = $('#type_discount').val();
    total = $('#totalVenta').val();
    newTotal = Math.round(total * (1-descuento));
    totalDescuento = Math.round(total * descuento);

    $('#total').html('Q.'+newTotal+'.00');
    $('#totalVenta').val(newTotal);
    $('#descuento').html(totalDescuento);
    $('#totalDescuento').val(totalDescuento);
  }
  $('#guardar').click(validar);
  function validar(){
    customer = $('#customer_id').val();
    operator = $('#operator_id').val();
    
    if(customer == null){
      alert('Por favor, ingrese un cliente');
      location.reload();
    }
  }
</script>
@endpush
@endsection
