<div class="modal fade" id="modal-pay-{{$sale->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning" >
                <h5 class="modal-title" id="exampleModalLabel">Abonar al Cliente {{$sale->customer->fullname}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{route('payment.store')}}" method="post">
                    @csrf
                    <input type="hidden" name="sale_traetment_id" value="{{$sale->id}}">
                    <div class="mb-3">
                        <label for="balance" class="form-label">Saldo</label>
                        <h4>Q. {{number_format($sale->balance,2)}}</h4><input type="hidden" id="balance-{{$sale->id}}" name="balance" value="{{$sale->balance}}">
                    </div>
                    <div class="mb-3">
                        <label for="balance" class="form-label">Abono # 1</label>
                        <h5>Q. {{number_format($sale->payment,2)}} | {{$sale->created_at}} <a href="{{action('SaleTraetmentController@pay_sale', $sale->id)}}"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i></button></a></h5>  

                    </div>
                    @if(isset($pays))
                        @foreach($pays as $pay)
                            @if($sale->id === $pay->sale_traetment_id)
                                <div class="mb-3">
                                    <label for="balance" class="form-label">Abono # {{$loop->iteration +2}}</label>
                                    <h5>Q. {{number_format($pay->payment,2)}}  | {{$pay->created_at}}</h5>  <a href="{{action('SaleTraetmentController@pay', $pay->id)}}"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i></button></a>

                                </div>  
                            @endif
                        @endforeach
                    @endif
                    <div class="mb-3">
                        <label for="payment" class="form-label">Abono</label>
                        <input type="text" style="font-size: 30px;" onchange="calcularsaldo({{$sale->id}})" class="form-control" id="payment-{{$sale->id}}" name="payment" placeholder="2000">
                    </div>
                    <div class="mb-3">
                        <label for="balance" class="form-label">Nuevo Saldo</label>
                        <h4 id="total-{{$sale->id}}"></h4><input type="hidden" id="newBalance-{{$sale->id}}" name="newBalance">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                        <button class="btn btn-primary">Abonar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
