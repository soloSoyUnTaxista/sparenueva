@extends('layouts.admin')
@section('content')
<div style="background: white" class="container">
  <hr><h2 class="text-center">Ventas</h2><hr>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-xs-12">  
          <label for="customer_id" class="form-label">Cliente</label>
          <select name="customer_id" id="customer_id" class="form-control">
            <option>{{$sale->customer->fullname}}</option>
          </select>
      </div>
      <div class="col-lg-4 col-md-4 col-xs-12">
        <label for="user_id" class="form-label">Usuario</label>
        <select name="user_id" id="user_id" class="form-control">
          <option>{{$sale->user->name}}</option>
        </select>
      </div>
      <div class="col-lg-4 col-md-4 col-xs-12">
        <label for="operator_id" class="form-label">Operador</label>
        <select name="operator_id" id="operator_id" class="form-control">
          <option>{{$sale->operator->fullname}}</option>
        </select>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-md-9 col-xs-12">
        <label for="description" class="form-label">Descripción</label>
        <textarea name="description" id="description" cols="30" rows="2" class="form-control">{{$sale->description}}</textarea>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12" id="table">        
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Tratamiento</th>
            <th scope="col">P. Venta</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Sub-Total</th>
          </tr>
        </thead>
        <tbody id="tblProducts">
          @foreach($details as $detail)
          <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$detail->traetment->name}}</td>
              <td>Q.{{number_format($detail->price_traetment,2)}}</td>
              <td>{{$detail->quantity}}</td>
              <td>Q.{{number_format($detail->sub_total,2)}}</td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4>Q.{{number_format($sale->total,2)}}</h4> </td>
          </tr>
          <tr>
            <td>Abono:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4>Q. {{number_format($sale->payment,2)}}</h4></td>
          </tr>
          <tr>
            <td>Saldo:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4>Q. {{number_format($sale->balance,2)}}</h4></td>
          </tr>
        </tfoot>
</table>
    </div>
</div>
@endsection