@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('bussiness.crear')
    <hr><h2 class="text-center">Listado de Empresas  <button data-bs-toggle="modal" data-bs-target="#createBussiness" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($bussiness))
            <tr>
                <th>No.</th>
                <th>Nombre de Empresa</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Opciones</th>
            </tr>
            @foreach($bussiness as $bus)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$bus->name}}</td>
                    <td>{{$bus->address}}</td>
                    <td>{{$bus->phone}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editBussiness-{{$bus->id}}" href="{{action('BussinessController@edit', $bus->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$bus->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('bussiness.edit')
                @include('bussiness.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection