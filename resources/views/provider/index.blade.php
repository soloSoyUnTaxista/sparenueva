@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('provider.crear')
    <hr><h2 class="text-center">Listado de Proveedores  <button data-bs-toggle="modal" data-bs-target="#createProvider" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($providers))
            <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Empresa</th>
                <th>Opciones</th>
            </tr>
            @foreach($providers as $provider)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$provider->fullname}}</td>
                    <td>{{$provider->address}}</td>
                    <td>{{$provider->phone}}</td>
                    <td>{{$provider->bussiness->name}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editProvider-{{$provider->id}}" href="{{action('ProviderController@edit', $provider->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        @if(Auth::user()->type_user->name == 'admin')
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$provider->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                        @endif
                    </td>
                </tr>
                @include('provider.edit')
                @include('provider.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection