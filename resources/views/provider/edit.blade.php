<div class="modal fade" id="editProvider-{{$provider->id}}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-warning" >
        <h5 class="modal-title" id="exampleModalLabel">Proveedor</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('provider.update',$provider->id)}}" method="post">
        <input name="_method" type="hidden" value="PATCH">
            @csrf
          <div class="mb-3">
            <label for="fullname" class="form-label">Nombre Completo</label>
            <input type="text" class="form-control" name="fullname" value="{{$provider->fullname}}">
          </div>
          <div class="mb-3">
            <label for="address" class="form-label">Dirección</label>
            <input type="text" class="form-control" name="address" value="{{$provider->address}}">
          </div>
          <div class="mb-3">
            <label for="phone" class="form-label">Teléfono</label>
            <input type="text" class="form-control" name="phone" value="{{$provider->phone}}">
          </div>
          <div class="mb-3">
            <label for="phone" class="form-label">Empresa</label>
            <select name="bussiness_id" id="bussiness_id" class="form-control ">
              @foreach($bussiness as $bus)
                <option value="{{$bus->id}}" {{($bus->id == $provider->bussiness_id) ? 'selected' : ''}}>{{$bus->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Editar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
