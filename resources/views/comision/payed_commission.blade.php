<div class="modal fade" id="payOperator-{{$operator->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-warning" >
          <h5 class="modal-title" id="exampleModalLabel">Confirmar Pago de Comisiones</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{route('comision.update',$operator->id)}}" method="post">
            <p>Confirma que desea Pagar las Comisiones de <b>{{$operator->fullname}}</b> </p>
            <p>Por un <b>Total de: </b> <h3>Q.{{number_format($total+$totalSales,2)}}</h3> </p>
          <input name="_method" type="hidden" value="PATCH">
              @csrf
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button class="btn btn-danger">Pagar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  