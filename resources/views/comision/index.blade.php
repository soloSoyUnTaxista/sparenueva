@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Comisiones</h2><hr>
    <table class="table text-center">
        @if(isset($operators))
            <tr>
                <th>No.</th>
                <th>Operador</th>
                <th>Teléfono</th>
            </tr>
            @foreach($operators as $operator)
                <tr>
                    <th>{{$loop->iteration}}</th>
                    <th><a href="{{route('comision.edit',$operator->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Comisiones" aria-hidden="true" class="btn btn-info"> {{$operator->fullname}}</button></a></th>
                    <th>{{$operator->phone}}</th>
                </tr>
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection