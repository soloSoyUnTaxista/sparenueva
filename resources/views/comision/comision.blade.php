@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Comisiones de {{$operator->fullname}}</h2><hr>
    <div class="text-center">
        @include('comision.payed_commission')
        <a data-bs-toggle="modal" data-bs-target="#payOperator-{{$operator->id}}" class="btn btn-warning">Pagar Comisiones</a>
    </div>
    <h2 class="text-center">Comisión por Tratamiento</h2>
    <table class="table text-center">
        @if(isset($traetment_process))
            <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Tratamiento</th>
                <th>Cantidad</th>
                <th>Estado</th>
                <th>Fecha</th>
            </tr>
            @foreach($traetment_process as $tp)
                @if($tp->commission->is_payed == 0)
                    <tr>
                        <th>{{$loop->iteration}}</th>
                        <th>{{$tp->sale_traetment->customer->fullname}}</th>
                        <th>{{$tp->traetment->name}}</th>
                        <th>Q.{{number_format($tp->commission->quantity,2)}}</th>
                        <th>Pendiente</th>
                        <th>{{$tp->commission->created_at}}</th>
                    </tr>
                @endif
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
    <h4>Total Comisiones Pendientes de Pago: Q.{{number_format($total,2)}}</h4>
    <hr>
    <h2 class="text-center">Comisión por Ventas</h2>
    <table class="table text-center">
        @if(isset($sales))
            <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Tratamiento</th>
                <th>Cantidad</th>
                <th>Estado</th>
                <th>Fecha</th>
            </tr>
            @foreach($sales as $sale)
                @if($sale->commission->is_payed == 0)
                    <tr>
                        <th>{{$loop->iteration}}</th>
                        <th>{{$sale->customer->fullname}}</th>
                        <th>{{$sale->total}}</th>
                        <th>Q.{{number_format($sale->commission->quantity,2)}}</th>
                        <th>Pendiente</th>
                        <th>{{$sale->created_at}}</th>
                    </tr>
                @endif
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
    <h4>Total Comisiones Pendientes de Pago: Q.{{number_format($totalSales,2)}}</h4>
</div>
@endsection