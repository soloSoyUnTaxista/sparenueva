<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'RENUEVA') }}</title>

        
        
        
        <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
        
        <!-- Animate.css -->
        <link rel="stylesheet" href="{{ asset('css/animate.css')}}">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="{{ asset('css/icomoon.css')}}">
        
        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        
        <!-- Flexslider  -->
        <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}">
        <!-- Flaticons  -->
        <link rel="stylesheet" href="{{asset('/fonts/flaticon/font/flaticon.css')}}">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{asset('/css/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{asset('/css/owl.theme.default.min.css')}}">
        <!-- Theme style  -->
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/select.min.css') }}">
        
        <!-- Modernizr JS -->
        <script src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
        <!-- FOR IE9 below -->
        <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="font-sans antialiased bg-light">
        <x-jet-banner />
        @livewire('navigation-menu')
        <x-jet-banner />
       <div class="container">
           <div class="row">
               <!-- aside de navegacion -->
               <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">  
                   
                <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>         
                   <aside id="colorlib-aside" role="complementary" class="border js-fullheight">
                       <br>
                       <h1 id="colorlib-logo"><a href="index.html">RENUEVA</a></h1>
                       <nav id="colorlib-main-menu" role="navigation">
                           <ul>
                               .<li class="colorlib-active"><a href="/event">Inicio</a></li>
                               .<li><a href="/customer">Clientes</a></li>
                               .<li><a href="/provider">Proveedores</a></li>
                               .<li><a href="/operator">Operadores</a></li>
                               .<li><a href="/bussiness">Empresas</a></li>
                               .<li><a href="/product">Productos</a></li>
                               .<li><a href="/sale">Ventas</a></li>
                               .<li><a href="/purchase">Compras</a></li>
                               .<li><a href="/sale_traetment">Venta de Tratamientos</a></li>
                               .<li><a href="/traetment_process">Tratamientos Pendientes</a></li>
                               .<li><a href="/comision">Comisiones</a></li>
                               .<li><a href="/traetment">Tratamientos</a></li>
                               .<li><a href="/quantity_session">Cantidad de Sesiones</a></li>
                               .<li><a href="/typeTraetment">Tipos de Tratamiento</a></li>
                               @if(Auth::user()->type_user->name == 'admin')
                               .<li><a href="/users">Usuarios</a></li>
                               @endif
                               .<li><a href="/expense">Gastos</a></li>
                               .<li><a href="/box">Caja</a></li>
                           </ul>
                       </nav>
           
                       <div class="colorlib-footer">
                            <p>
                                <small>&copy; Copyright Reserved&copy;<script>document.write(new Date().getFullYear());</script>  by Diseños y Sistemas Ramirez </small>
                            </p>
                           <ul>
                               <li><a href="#"><i class="icon-facebook2"></i></a></li>
                               <li><a href="#"><i class="icon-twitter2"></i></a></li>
                               <li><a href="#"><i class="icon-instagram"></i></a></li>
                               <li><a href="#"><i class="icon-linkedin2"></i></a></li>
                           </ul>
                       </div>
                   </aside>
               </div>
               <div class="col-lg-10 col-md-10 col-xs-12">
                   <!-- Page Content -->
                   <!-- Main content -->
                   <aside id="colorlib-hero" class="js-fullheight">
                        @yield('content')                     
                   </aside><!-- /.content -->
                 <!--Fin-Contenido-->
               </div>
           </div>
       </div>

        @stack('modals')
        @livewireScripts

        <!-- jQuery -->
        <script src="{{asset('js/jquery.min.js')}}"></script>
        @stack('scripts')
        <!-- jQuery Easing -->
        <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
        <!-- Waypoints -->
        <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
        <!-- Flexslider -->
        <script src="{{asset('js/jquery.flexslider-min.js')}}"></script>
        <!-- Sticky Kit -->
        <script src="{{asset('js/sticky-kit.min.js')}}"></script>
        <!-- Owl carousel -->
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        <!-- Counters -->
        <script src="{{asset('js/jquery.countTo.js')}}"></script>
        

        
        
        <!-- MAIN JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>
    
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    </body>
</html>
