<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>RENUEVA</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/styles.css')}}">
        <link href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css' rel='stylesheet'>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />

        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="/event">RENUEVA</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    
                </div>
            </form>
            <!-- Navbar-->
            @auth
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#!">Configuración</a></li>
                        <li><a class="dropdown-item" href="{{route('profile.show')}}">Perfil</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><!-- Authentication -->
                          <x-jet-dropdown-link href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                              {{ __('Cerrar Sesión') }}
                          </x-jet-dropdown-link>
                          <form method="POST" id="logout-form" action="{{ route('logout') }}">
                              @csrf
                          </form>
                        </li>
                    </ul>
                </li>
            </ul>
            @endauth
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Renueva</div>
                            <a class="nav-link" href="/event">
                                <div class="sb-nav-link-icon"><i class="fas fa-calendar"></i></div>
                                Calendario
                            </a>
                            <a class="nav-link" href="/customer">
                                <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                                Clientes
                            </a>
                            <div class="sb-sidenav-menu-heading">Tratamiento</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Tratamiento
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="/traetment">Tratamiento</a>
                                    <a class="nav-link" href="/traetment_process">Proceso de Tratamiento</a>
                                    <a class="nav-link" href="/typeTraetment">Tipo de Tratamiento</a>
                                    <a class="nav-link" href="/quantity_session">Sesiones por Tratamiento</a>
                                </nav>
                            </div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages">
                                <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                                Venta
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                                        Tratamiento
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="/sale_traetment">Venta de Tratamiento</a>
                                            <a class="nav-link" href="/operator">Operadores</a>
                                            <a class="nav-link" href="/comision">Comisión</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">
                                        Productos
                                        <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                                    </a>
                                    <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordionPages">
                                        <nav class="sb-sidenav-menu-nested nav">
                                            <a class="nav-link" href="/sale">Venta de Productos</a>
                                            <a class="nav-link" href="/product">Productos</a>
                                        </nav>
                                    </div>
                                    <a class="nav-link" href="/comision">
                                        <div class="sb-nav-link-icon"><i class="fas fa-calendar"></i></div>
                                        Comisiones
                                    </a>
                                </nav>
                            </div>
                            <div class="sb-sidenav-menu-heading">Compras</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#shopping" aria-expanded="false" aria-controls="collapseLayouts">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Compras
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="shopping" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="/purchase">Compras</a>
                                    <a class="nav-link" href="/bussiness">Empresa</a>
                                    <a class="nav-link" href="/provider">Proveedor</a>
                                </nav>
                            </div>
                            
                            <div class="sb-sidenav-menu-heading">Finanzas</div>
                            <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseFinanzas" aria-expanded="false" aria-controls="collapseFinanzas">
                                <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                                Caja
                                <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                            </a>
                            <div class="collapse" id="collapseFinanzas" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                                <nav class="sb-sidenav-menu-nested nav">
                                    <a class="nav-link" href="/box">
                                        <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                        Cierre de Caja
                                    </a>
                                    <a class="nav-link" href="/expense">
                                        <div class="sb-nav-link-icon"><i class="fas fa-money-bill"></i></div>
                                        Gastos
                                    </a>
                                    <a class="nav-link" href="/cuadre">
                                        <div class="sb-nav-link-icon"><i class="fas fa-money-bill"></i></div>
                                        Cuadres de Caja
                                    </a>
                                    @if(Auth::user()->type_user->name == 'admin')
                                    <a class="nav-link" href="/listCuadre">
                                        <div class="sb-nav-link-icon"><i class="fas fa-money-bill"></i></div>
                                        Listado de Cuadres
                                    </a>
                                    @endif
                                </nav>
                            </div>
                            @if(Auth::user()->type_user->name == 'admin')
                            <div class="sb-sidenav-menu-heading">Acceso</div>
                            <a class="nav-link" href="/users">
                                <div class="sb-nav-link-icon"><i class="fas fa-user"></i></div>
                                Usuarios
                            </a>
                            @endif
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">Usuario Logueado:</div>
                        {{Auth::user()->name}}
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container">
                        @yield('content')
                    </div>
                </main>
                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">Copyright &copy; Design and System Ramirez 2022-2024</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        @stack('modals')
        @livewireScripts
        <script src="{{asset('js/jQuery.min.js')}}"></script>
        @stack('scripts')
        <script src="{{asset('js/app.js')}}" defer></script>
        <script src="{{asset('js/scripts.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/fullcalendar.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        
    </body>
</html>
