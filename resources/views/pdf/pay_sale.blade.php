<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        table {
          border-collapse: collapse;
          width: 100%;
        }
        
        th, td {
          padding: 8px;
          text-align: left;
          border-bottom: 1px solid #ddd;
        }
        #marco{
            border: 10px solid #edd711;
            padding: 5%
        }
    </style>
</head>
<body>
<div id="marco" style="text-align: center">
    <img src="images/logo.jpg" style="width:15%" alt="logotipo"> 
    
    <h4>Cliente: {{$customer}} &nbsp;&nbsp;&nbsp; Fecha de Pago: {{$sale->created_at}}</h4>
    <table>
        <tr>
            <th scope="col">Saldo Ant.</th>
            <th scope="col">Saldo Act.</th>
            <th scope="col">Abono</th>
        </tr>
        
        <tr>
            <td>Q.{{number_format($sale->total-$sale->totalDiscount,2)}}</td>
            <td>Q.{{number_format($sale->balance,2)}}</td>
            <td>Q.{{number_format($sale->payment,2)}}</td>
        </tr>
        
        <tr>
            <td>Total Abono:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4>Q.{{number_format($sale->payment,2)}}</h4> </td>
        </tr>
    </table>
</div>
</body>
</html>