<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        table {
          border-collapse: collapse;
          width: 100%;
        }
        
        th, td {
          padding: 8px;
          text-align: left;
          border-bottom: 1px solid #ddd;
        }
        #marco{
            border: 10px solid #edd711;
            padding: 5%
        }
    </style>
</head>
<body>
<div id="marco" style="text-align: center">
    <img src="images/logo.jpg" style="width:15%" alt="logotipo"> 
    
    <h4>Cliente: {{$sale->customer->fullname}} &nbsp;&nbsp;&nbsp; Fecha de Venta: {{$sale->created_at}}</h4>
    <table>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tratamiento</th>
            <th scope="col">Precio</th>
            <th scope="col">Cantidad</th>
            <th scope="col">Sub-Total</th>
        </tr>
        @foreach($details as $detail)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$detail->traetment->name}}</td>
            <td>Q.{{number_format($detail->price_traetment,2)}}</td>
            <td>{{$detail->quantity}}</td>
            <td>Q.{{number_format($detail->sub_total,2)}}</td>
        </tr>
        @endforeach
        <tr>
            <td>Total:</td>
            <td></td>
            <td></td>
            <td></td>
            <td><h4>Q.{{number_format($sale->total,2)}}</h4> </td>
        </tr>
    </table>
</div>
</body>
</html>