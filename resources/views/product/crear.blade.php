<div class="modal fade" id="createProduct">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Producto</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
            @csrf
          <div class="mb-3">
            <label for="name" class="form-label">Producto</label>
            <input type="text" class="form-control" name="name" placeholder="Crema Sesderma 300ml" required>
          </div>
          <div class="mb-3">
            <label for="address" class="form-label">Stock</label>
            <input type="text" class="form-control" name="stock" id="stock" onkeypress="return validar(event);" placeholder="0" required>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6 col-xs-12">
              <div class="mb-3">
                <label for="phone" class="form-label">Precio de Compra</label>
                <input type="text" class="form-control" name="price_purchase" id="price_purchase" onkeypress="return validar(event);" value="0" required>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12">
              <div class="mb-3">
                <label for="phone" class="form-label">Precio de Venta</label>
                <input type="text" class="form-control" name="price_sale" id="price_sale" onkeypress="return validar(event);" placeholder="0.00" required>
              </div>
            </div>
          </div>
          <div class="mb-3 text-center">
            <img id="imageSelected" style="max-height: 100px;">
          </div>
          <div class="mb-3">
            <label for="profile_picture" class="form-label">Foto de Perfil</label>
            <input type="file" class="form-control" name="profile_picture" id="profile_picture">
          </div>
          <div class="mb-3">
            <label for="phone" class="form-label">Descripción</label>
            <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
              <button class="btn btn-primary">Guardar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@push('scripts')

<script>
  $(document).ready(function(e){
    $('#profile_picture').change(function(){
      let reader = new FileReader();
      reader.onload = (e) => {
        $('#imageSelected').attr('src',e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    });
  });
  function validar(evt)
  {
      var code = (evt.which) ? evt.which : evt.keyCode;
      if(code==8){
          return true;
      }else if(code>=46 && code<=57){
          return true;
      }else{
          return false;
      }
  }
</script>
@endpush
