<div class="modal fade" id="editProduct-{{$product->id}}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-warning" >
        <h5 class="modal-title" id="exampleModalLabel">Editar Producto</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{route('product.update',$product->id)}}" method="post">
        <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="mb-3">
              <label for="name" class="form-label">Producto</label>
              <input type="text" class="form-control" name="name" value="{{$product->name}}" required>
            </div>
            <div class="mb-3">
              <label for="address" class="form-label">Stock</label>
              <input type="text" class="form-control" name="stock" id="stock" value="{{$product->stock}}" onkeypress="return validar(event);" required>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="mb-3">
                  <label for="phone" class="form-label">Precio de Compra</label>
                  <input type="text" class="form-control" name="price_purchase" id="price_purchase" value="{{$product->price_purchase}}" onkeypress="return validar(event);" required>                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="mb-3">
                  <label for="phone" class="form-label">Precio de Venta</label>
                  <input type="text" class="form-control" name="price_sale" id="price_sale" value="{{$product->price_sale}}" onkeypress="return validar(event);" required>
                </div>
              </div>
            </div>
            <div class="mb-3">
              <label for="phone" class="form-label">Foto de Perfil</label>
              <input type="file" class="form-control" name="profile_picture">
            </div>
            <div class="mb-3">
              <label for="phone" class="form-label">Descripción</label>
              <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{$product->description}}</textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button class="btn btn-primary">Guardar</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
