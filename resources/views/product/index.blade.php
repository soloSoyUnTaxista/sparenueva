@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('product.crear')
    <hr><h2 class="text-center">Listado de Productos  <button data-bs-toggle="modal" data-bs-target="#createProduct" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($products))
            <tr>
                <th>No.</th>
                <th>Producto</th>
                <th>Stock</th>
                <th>P.Compra</th>
                <th>P.Venta</th>
                <th>Descripción</th>
                <th>F. Perfil</th>
                <th>Opciones</th>
            </tr>
            @foreach($products as $product)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->stock}}</td>
                    <td>Q.{{number_format($product->price_purchase,2)}}</td>
                    <td>Q.{{number_format($product->price_sale,2)}}</td>
                    <td>{{$product->description}}</td>
                    <td>
                        <img src="{{$product->profile_picture}}" width="60%" alt="">
                    </td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editProduct-{{$product->id}}" href="{{action('ProductController@edit', $product->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info btn-sm"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$product->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('product.edit')
                @include('product.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection