@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('traetment.crear')
    <hr><h2 class="text-center">Listado de Tratamientos  <button data-bs-toggle="modal" data-bs-target="#createTraetment" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <input type="text" class="form-control" id="searchTraetment" placeholder="Tratamiento a Buscar">
    <table class="table text-center" id="tableData">
        @if(isset($traetments)) 
            <tr>
                <th>No.</th>
                <th>Tratamiento</th>
                <th>Precio</th>
                <th>C. de Sesiones</th>
                <th>Tipo de Tratamiento</th>
                <th>Descripción</th>
            </tr>
            @foreach($traetments as $traetment)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$traetment->name}}</td>
                    <td>Q.{{number_format($traetment->price_traetment,2)}}</td>
                    <td>{{$traetment->quantity_session->quantity_session}} - {{$traetment->quantity_session->time_frame}}</td>
                    <td>{{$traetment->type_traetment->name}}</td>
                    <td>{{$traetment->description}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editTraetment-{{$traetment->id}}" href="{{action('TraetmentController@edit', $traetment->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$traetment->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('traetment.edit')
                @include('traetment.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@push('scripts')
<script>
    $(document).ready(function(){
            $('#searchTraetment').on("keyup",function(){
                var value = $(this).val().toLowerCase();
                $('#tableData tr').filter(function(){
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
  function validar(evt)
  {
      var code = (evt.which) ? evt.which : evt.keyCode;
      if(code==8){
          return true;
      }else if(code>=48 && code<=57){
          return true;
      }else{
          return false;
      }
  }
</script>
@endpush
@endsection