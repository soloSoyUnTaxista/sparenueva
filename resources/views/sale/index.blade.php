@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Ventas <a href="{{route('sale.create')}}" class="btn btn-success" > <i class="fa fa-plus"></i></a></h2><hr>
    <table class="table text-center">
        @if(isset($sales))
            <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Teléfono</th>
                <th>Total</th>
                <th>Descripción</th>
                <th>Opciones</th>
            </tr>
            @foreach($sales as $sale)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$sale->customer->fullname}}</td>
                    <td>{{$sale->customer->phone}}</td>
                    <td>Q.{{number_format($sale->total,2)}}</td>
                    <td>{{$sale->description}}</td>
                    <td>
                        <a href="{{action('SaleController@show', $sale->id)}}"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>   
                        <a href="{{action('SaleController@imprimir', $sale->id)}}"><button class="btn btn-info btn-sm"><i class="fa fa-print" aria-hidden="true"></i></button></a>
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$sale->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger btn-sm"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                    </td>
                </tr>
                @include('sale.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection