@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Listado de Tratamientos  <button data-bs-toggle="modal" data-bs-target="#createTraetment" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <input type="text" class="form-control" id="searchCustomer" placeholder="Cliente a Buscar...">
    <table class="table text-center">
        @if(isset($customers)) 
            <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Teléfono</th>
            </tr>
            @foreach($customers as $customer)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$customer->fullname}}</td>
                    <td>{{$customer->phone}}</td>
                    <td>
                        <a href="{{route('traetment_process.edit',$customer->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info">Ver Tratamientos</button></a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
    {{$customers->links()}}
</div>
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        $('#searchCustomer').on("keyup",function(){
            var value = $(this).val().toLowerCase();
            $('#tableData tr').filter(function(){
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
@endpush