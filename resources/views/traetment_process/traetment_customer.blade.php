@extends('layouts.admin')
@section('content')
<div style="background: white">
    <hr><h2 class="text-center">Procedimiento de Tratamientos</h2><hr>
    <input type="text" class="form-control" id="searchTraetment" placeholder="Tratamiento a Buscar">
    <table class="table text-center" id="tableData">
        @if(isset($traetments)) 
            <tr>
                <th>No.</th>
                <th>Cliente</th>
                <th>Tratamiento</th>
                <th>Precio</th>
                <th>C. de Sesiones</th>
                <th>Estado</th>
            </tr>
            @foreach($traetments as $traetment)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$traetment->customer->fullname}}</td>
                    <td>{{$traetment->traetment->name}}</td>
                    <td>Q.{{number_format($traetment->price,2)}}</td>
                    <td>{{$traetment->num_session}}</td>
                    <td>@if($traetment->state == 1) Finalizado @else<a data-bs-toggle="modal" data-bs-target="#editTraetment-{{$traetment->id}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info">Finalizar</button></a>@endif</td>
                </tr>
                @include('traetment_process.process')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
    {{$traetments->links()}}
</div>
@push('scripts')
<script>
    $(document).ready(function(){
        $('#searchTraetment').on("keyup",function(){
            var value = $(this).val().toLowerCase();
            $('#tableData tr').filter(function(){
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
@endpush
@endsection