<div class="modal fade" id="editTraetment-{{$traetment->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-danger" >
          <h5 class="modal-title" id="exampleModalLabel">Finalizar Sesión</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <form action="{{route('traetment_process.update',$traetment->id)}}" method="post">
            <input name="_method" type="hidden" value="PATCH">
            @csrf
            <div class="mb-3">
              <label for="name" class="form-label">Cliente</label>
              <input type="text" class="form-control" name="name" value="{{$traetment->sale_traetment->customer->fullname}}">
            </div>
            <div class="mb-3">
              <label for="name" class="form-label">Nombre de Tratamiento</label>
              <input type="text" class="form-control" name="name" value="{{$traetment->traetment->name}}">
            </div>
            <div class="mb-3">
              <label for="num_session" class="form-label"># de Sesión</label>
              <input type="text" class="form-control" name="num_session" value="{{$traetment->num_session}}">
            </div>
            <div class="mb-3">
              <label for="name" class="form-label">Operador</label>
              <select name="operator_id" id="operator_id" class="form-control" required>
                <option disabled selected value="">--Elegir un Operador--</option>
                @foreach($operators as $operator)
                  <option value="{{$operator->id}}">{{$operator->fullname}}</option>
                @endforeach
              </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button class="btn btn-primary">Finalizar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  