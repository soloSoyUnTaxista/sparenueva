@extends('layouts.admin')
@section('content')
<div style="background: white">
    @include('operator.crear')
    <hr><h2 class="text-center">Listado de Operadores  <button data-bs-toggle="modal" data-bs-target="#createOperator" class="btn btn-success rounded-circle text-white"><i class="fa fa-plus"></i></button></h2><hr>
    <table class="table text-center">
        @if(isset($operators))
            <tr>
                <th>No.</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Opciones</th>
            </tr>
            @foreach($operators as $operator)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$operator->fullname}}</td>
                    <td>{{$operator->address}}</td>
                    <td>{{$operator->phone}}</td>
                    <td>
                        <a data-bs-toggle="modal" data-bs-target="#editOperator-{{$operator->id}}" href="{{action('OperatorController@edit', $operator->id)}}"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Editar" aria-hidden="true" class="btn btn-info"><i class="fas fa-edit" aria-hidden="true"></i></button></a>
                        @if(Auth::user()->type_user->name == 'admin')
                        <a data-bs-toggle="modal" data-bs-target="#modal-delete-{{$operator->id}}" data-toggle="modal"><button data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> </button></a>   
                        @endif
                    </td>
                </tr>
                @include('operator.edit')
                @include('operator.delete')
            @endforeach
        @else
            <tr>
                <td>No Existen Registros</td>
            </tr>
        @endif
    </table>
</div>
@endsection