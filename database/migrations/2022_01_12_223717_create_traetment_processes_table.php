<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraetmentProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traetment_processes', function (Blueprint $table) {
            $table->id();
            $table->integer('num_session');
            $table->integer('price');
            $table->tinyinteger('state')->default('0');
            $table->bigInteger('sale_traetment_id')->unsigned();
            $table->bigInteger('traetment_id')->unsigned();
            $table->bigInteger('operator_id')->unsigned()->nullable();
            $table->bigInteger('commission_id')->unsigned()->nullable();
            $table->bigInteger('customer_id')->unsigned()->nullable();

            $table->foreign('sale_traetment_id')->references('id')->on('sale_traetments');
            $table->foreign('traetment_id')->references('id')->on('traetments');
            $table->foreign('operator_id')->references('id')->on('operators');
            $table->foreign('commission_id')->references('id')->on('commissions');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traetment_processes');
    }
}
