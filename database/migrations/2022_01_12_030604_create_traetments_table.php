<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraetmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traetments', function (Blueprint $table) {
            $table->id();
            $table->string('name',200);
            $table->float('price_traetment');
            $table->text('description')->nullable();
            $table->bigInteger('quantity_session_id')->unsigned();
            $table->bigInteger('type_traetment_id')->unsigned();
            $table->timestamps();

            $table->foreign('quantity_session_id')->references('id')->on('quantity_sessions');
            $table->foreign('type_traetment_id')->references('id')->on('type_traetments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traetments');
    }
}
