<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailSaleTraetmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_sale_traetments', function (Blueprint $table) {
            $table->id();
            $table->integer('quantity');
            $table->integer('discount');
            $table->float('price_traetment');
            $table->float('sub_total');
            $table->bigInteger('sale_traetment_id')->unsigned();
            $table->bigInteger('traetment_id')->unsigned();
            $table->timestamps();

            $table->foreign('sale_traetment_id')->references('id')->on('sale_traetments');
            $table->foreign('traetment_id')->references('id')->on('traetments');        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_sale_traetments');
    }
}
