<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use App\Models\Cuadre;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cuadre = Cuadre::orderBy('id','desc')->first();
        $fecha_fin = $cuadre->end;
        $expenses = Expense::whereDate('created_at','>',$fecha_fin)->get();
        return view('expense.index',compact('expenses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Expense::create($request->all());
        return redirect ('expense');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $expense = Expense::findOrFail($id);
        $expense->update($request->all());
        return redirect('expense');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $expense = Expense::findOrFail($id);
        $expense->delete();
        return redirect('expense');
    }
}
