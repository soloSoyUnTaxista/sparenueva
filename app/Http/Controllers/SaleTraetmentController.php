<?php

namespace App\Http\Controllers;

use App\Models\SaleTraetment;
use App\Models\Traetment;
use App\Models\TypeTraetment;
use App\Models\DetailSaleTraetment;
use App\Models\TraetmentProcess;
use App\Models\Customer;
use App\Models\Payment;
use App\Models\Operator;
use Illuminate\Http\Request;
use PDF;

class SaleTraetmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sales = SaleTraetment::all();
        $pays = Payment::all();
        return view('sale_traetment.index',compact('sales','pays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $traetments = Traetment::all();
        $typeTraetments = TypeTraetment::all();
        $customers = Customer::all();
        $operators = Operator::all();
        return view('sale_traetment.crear',compact('traetments','customers','operators','typeTraetments'));
    }
    public function pay_sale($id){
        $sale = SaleTraetment::findOrFail($id);
        $customer = $sale->customer->fullname;
        
        $pdf = PDF::loadView('pdf.pay_sale',compact('sale','customer'));
        $pdf->setPaper('a5','landscape');
        return $pdf->download('Pago - '.$customer.'.pdf');
    }
    public function pay($id){
        $pay = Payment::findOrFail($id);
        $customer = $pay->sale_traetment->customer->fullname;
        
        $pdf = PDF::loadView('pdf.pay',compact('pay','customer'));
        $pdf->setPaper('a5','landscape');
        return $pdf->download('Pago - '.$customer.'.pdf');
    }

    public function imprimir($id){
        $sale = SaleTraetment::findOrFail($id);
        $customer = $sale->customer->fullname;

        $details = DetailSaleTraetment::where('sale_traetment_id',$id)->get();

        $pdf = PDF::loadView('pdf.sale_traetment',compact('sale','details'));
        $pdf->setPaper('a5','landscape');
        return $pdf->download('Venta a '.$customer.'.pdf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = $request->get('user_id');
        try {
            //code...
            //data for saved sale_traetment
            $num_max = SaleTraetment::select('num_document')->max('num_document');
            $num_document = ($num_max + 1);
            
            $total = $request->get('total');
            $payment = $request->get('payment');
            $balance = $request->get('balance');
            $description = $request->get('description');
            $customer = $request->get('customer_id');
            $operador = $request->get('operator_id');
            $user = $request->get('user_id');
            $totalDescuento = $request->get('totalDescuento');

            $sale = new SaleTraetment();
            $sale->num_document = $num_document;
            $sale->total = $total;
            $sale->payment = $payment;
            $sale->balance = $balance;
            $sale->description = $description;
            $sale->customer_id = $customer;
            $sale->operator_id = $operador;
            $sale->user_id = $user;
            $sale->totalDiscount = $totalDescuento;
            $sale->save();
            
            //data for saved detail_sale
            $traetment = $request->get('traetment_id');
            $quantity = $request->get('quantity');
            $price = $request->get('price_traetment');
            $subTotal = $request->get('subTotal');
            $discount = $request->get('discount');
    
            $cont = 0;
            while($cont < count($traetment)){

                $detail = new DetailSaleTraetment();
                $detail->quantity = $quantity[$cont];
                $detail->price_traetment = $price[$cont];
                $detail->sub_total = $subTotal[$cont];
                $detail->sale_traetment_id = $sale->id;
                $detail->traetment_id = $traetment[$cont];
                $detail->save();

                
                $traet = Traetment::findOrFail($traetment[$cont]);
                $quan = $traet->quantity_session->quantity_session;
                $price_traetment = ($price[$cont]/$quan);

                $contTraetment = 0;
                while($contTraetment < $quan){
                    $traetment_process = new TraetmentProcess();
                    $traetment_process->num_session = $contTraetment+1;
                    $traetment_process->price = $price_traetment;
                    $traetment_process->sale_traetment_id = $sale->id;
                    $traetment_process->traetment_id = $traetment[$cont];
                    $traetment_process->customer_id = $customer;
                    $traetment_process->save();

                    $contTraetment++;
                }

                $cont++;
            }
            return redirect('sale_traetment');
        } catch (\Throwable $th) {
            throw $th;
            return redirect('sale_traetment');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SaleTraetment  $saleTraetment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $sale = SaleTraetment::findOrFail($id);
        $details = DetailSaleTraetment::where('sale_traetment_id',$id)->get();
        return view('sale_traetment.show',compact('sale','details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SaleTraetment  $saleTraetment
     * @return \Illuminate\Http\Response
     */
    public function edit(SaleTraetment $saleTraetment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SaleTraetment  $saleTraetment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SaleTraetment $saleTraetment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SaleTraetment  $saleTraetment
     * @return \Illuminate\Http\Response
     */
    public function d                                                                                                                                                        (SaleTraetment $saleTraetment)
    {
        //
    }
}
