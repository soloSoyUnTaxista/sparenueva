<?php

namespace App\Http\Controllers;

use App\Models\TypeTraetment;
use Illuminate\Http\Request;

class TypeTraetmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $typeTraetments = TypeTraetment::all();
        return view('typeTraetment.index',compact('typeTraetments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        TypeTraetment::create($request->all());
        return redirect ('typeTraetment');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TypeTraetment  $typeTraetment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $typeTraetment = TypeTraetment::findOrFail($id);
        $typeTraetment->update($request->all());
        return redirect('typeTraetment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypeTraetment  $typeTraetment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $typeTraetment = TypeTraetment::findOrFaiL($id);
        $typeTraetment->delete();
        return redirect('typeTraetment');
    }
}
