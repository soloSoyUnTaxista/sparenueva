<?php

namespace App\Http\Controllers;

use App\Models\TraetmentProcess;
use App\Models\Operator;
use App\Models\Commission;
use App\Models\Customer;
use App\Models\SaleTraetment;

use Illuminate\Http\Request;

class TraetmentProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $customers = Customer::paginate(20);
        
        $traetments = TraetmentProcess::where('state',0)->paginate(10);
        return view('traetment_process.index',compact('customers'));
    }

    public function edit($id){
        $operators = Operator::all();
        $traetments = TraetmentProcess::where('customer_id',$id)->paginate(30);
        return view('traetment_process.traetment_customer',compact('traetments','operators'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TraetmentProcess  $traetmentProcess
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $traetmentProcess = TraetmentProcess::findOrFail($id);

        $price = $traetmentProcess->price; 
        $porcentaje = $traetmentProcess->traetment->type_traetment->porcentaje;
        $idTraetment = $traetmentProcess->traetment->id;
        if($idTraetment == 21){
            $commission = 110;
        }elseif ($idTraetment == 22){
            $commission = 220;
        }else{
            $porcentaje = ($porcentaje/100);
            $commission = ($price * $porcentaje);
        }
        $comision = new Commission();
        $comision->quantity = ceil($commission);
        $comision->save();

        $operator = $request->get('operator_id');

        $traetmentProcess->operator_id = $operator;
        $traetmentProcess->state = 1;
        $traetmentProcess->commission_id = $comision->id;
        $traetmentProcess->update();

        
        return redirect('traetment_process');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TraetmentProcess  $traetmentProcess
     * @return \Illuminate\Http\Response
     */
    public function destroy(TraetmentProcess $traetmentProcess)
    {
        //
    }
}
