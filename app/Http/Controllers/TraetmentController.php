<?php

namespace App\Http\Controllers;

use App\Models\Traetment;
use App\Models\QuantitySession;
use App\Models\TypeTraetment;
use Illuminate\Http\Request;

class TraetmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $traetments = Traetment::all();
        $quantities = QuantitySession::all();
        $typesTraetments = TypeTraetment::all();
        return view('traetment.index',compact('traetments','quantities','typesTraetments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Traetment::create($request->all());
        return redirect('traetment');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Traetment  $traetment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $traetment = Traetment::findOrFail($id);
        $traetment->update($request->all());
        return redirect('traetment');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Traetment  $traetment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $traetment = Traetment::findOrFail($id);
        $traetment->delete();
        return redirect('traetment');
    }
}
