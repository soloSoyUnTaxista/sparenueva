<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\DetailSale;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Operator;
use App\Models\Commission;
use Illuminate\Http\Request;
use PDF;


class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sales = Sale::orderBy('id','desc')->get();
        return view('sale.index',compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $customers = Customer::all();
        $products = Product::all();
        $operators = Operator::all();
        return view('sale.crear',compact('customers','products','operators'));
    }

    public function imprimir($id){
        $sale = Sale::findOrFail($id);
        $customer = $sale->customer->fullname;

        $details = DetailSale::where('sale_id',$id)->get();

        $pdf = PDF::loadView('pdf.sale',compact('sale','details'));
        $pdf->setPaper('a5','landscape');
        return $pdf->download('Venta a '.$customer.'.pdf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Datos para almacenar la Venta
        $customer = $request->get('customer_id');
        $operador = $request->get('operator_id');
        $user = $request->get('user_id');
        $total = $request->get('total');
        $description = $request->get('description');
        $num_document = Sale::select('num_document')->max('num_document');

        $comision = new Commission();
        $comision->quantity = ($total * 0.03);
        $comision->save();

        try {
            //code...
            $sale = new Sale();
            $sale->num_document = $num_document + 1;
            $sale->total = $total;
            $sale->description = $description;
            $sale->customer_id = $customer;
            $sale->operator_id = $operador;
            $sale->user_id = $user;
            $sale->commission_id = $comision->id;
            $sale->save();
    
            $product_id = $request->get('product_id');
            $quantity = $request->get('quantity');
            $price_sale = $request->get('price_sale');
            $subTotal = $request->get('subTotal');
    
            $cont = 0;
    
            while($cont < count($product_id)){

                $product = Product::findOrFaiL($product_id[$cont]);
                $stock = $product->stock;
                $product->stock = $stock - $quantity[$cont];
                $product->update();

                $detail = new DetailSale();
                $detail->quantity = $quantity[$cont];
                $detail->price_sale = $price_sale[$cont];
                $detail->sub_total = $subTotal[$cont];
                $detail->product_id = $product_id[$cont];
                $detail->sale_id = $sale->id;
                $detail->save();
    
                $cont = $cont + 1;
            }
            return redirect('sale');
        } catch (\Throwable $th) {
            throw $th;
            return redirect()->route('sale.create');
            echo '<script>alert("No se Realizó la Venta, Compruebe que lleno todos los datos")</script>';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $sale = Sale::findOrFail($id);

        $details = DetailSale::where('sale_id',$id)->get();
        return view('sale.show',compact('sale','details'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sale = Sale::findOrFail($id);
        $sale->delete();
        return redirect('sale');
    }
}
