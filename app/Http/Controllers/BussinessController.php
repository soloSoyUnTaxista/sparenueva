<?php

namespace App\Http\Controllers;

use App\Models\Bussiness;
use Illuminate\Http\Request;

class BussinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $bussiness = Bussiness::all();
        return view('bussiness.index',compact('bussiness'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Bussiness::create($request->all());
        return redirect('bussiness');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Bussiness  $bussiness
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $bussiness = Bussiness::findOrFail($id);
        $bussiness->update($request->all());
        return redirect('bussiness');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bussiness  $bussiness
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $bussiness = Bussiness::findOrFail($id);
        $bussiness->delete();
        return redirect('bussiness');
    }
}
