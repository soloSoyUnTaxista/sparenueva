<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request->ajax()){
            $data = Event::whereDate('start', '>=', $request->start)
            ->whereDate('end', '<=', $request->end)
            ->get(['id','title','start','end']);

            return response()->json($data);
        }

        return view('event.index');
    }

    public function Ajax(Request $request){
        switch ($request->type){
            case 'add':
                $event = Event::create([
                    'title' => $request->title,
                    'start' => $request->start,
                    'end' => $request->end,
                ]);
                return response()->json($event);
                break;
            
            case 'update':
                $event = Event::findOrFail($request->id)->update($request->all());

                return response()->json($event);
            break;

            case 'delete':
                $event = Event::findOrFail($request->id)->delete();

                return response()->json($event);
            break;

            default:
                break;
        }
    }

}
