<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sale;
use App\Models\SaleTraetment;
use App\Models\Payment;
use App\Models\Expense;
use App\Models\Commission;
use App\Models\Cuadre;
use App\Models\TraetmentProcess;
use Carbon\Carbon;

class BoxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $now = Carbon::create($request->get('fecha'))->format('Y-m-d');
        $sales = Sale::whereDate('created_at',$now)->get();
        $total_sales = $sales->sum('total');

        $sale_traetments = SaleTraetment::whereDate('created_at',$now)->get();
        $total_sales_traetments = $sale_traetments->sum('payment');

        $expenses = Expense::whereDate('created_at',$now)->get();
        $total_expenses = $expenses->sum('quantity');

        $pays = Payment::whereDate('created_at',$now)->get();
        $total_pays = $pays->sum('payment');

        $traetments_process = TraetmentProcess::where('state',1)->get();

        $commissions = Commission::whereDate('updated_at',$now)->where('is_payed',1)->get();
        $total_comisiones = $commissions->sum('quantity');

        $ventas = Sale::all();
        
        $total = ($total_sales + $total_sales_traetments + $total_pays);
        $total_egresos = $total_comisiones + $total_expenses;
        
        return view('box.box',compact('sales','sale_traetments','pays','total_pays','total_sales_traetments','total_sales','expenses','total_expenses','total','traetments_process','now','total_comisiones','total_egresos','ventas'));
    }

    public function cuadre_caja(Request $request){

        $inicio = Carbon::create($request->get('start'));
        $inicio->format('Y-m-d');

        $final = Carbon::create($request->get('end'));
        $final->addDay()->format('Y-m-d');
        
        $sales = Sale::whereBetween('created_at', [$inicio, $final]  )->get();
        $total_sales = $sales->sum('total');
        
        $sale_traetments = SaleTraetment::whereBetween('created_at',[$inicio, $final])->get();
        $total_sales_traetments = $sale_traetments->sum('payment');
        
        $expenses = Expense::whereBetween('created_at',[$inicio, $final])->get();
        $total_expenses = $expenses->sum('quantity');
        
        $pays = Payment::whereBetween('created_at',[$inicio, $final])->get();
        $total_pays = $pays->sum('payment');
        
        $traetments_process = TraetmentProcess::where('state',1)->get();
        
        $commissions = Commission::whereBetween('updated_at',[$inicio, $final])->where('is_payed',1)->get();
        
        $total_comisiones = $commissions->sum('quantity');
        

        $ventas = Sale::all();
        
        $total = ($total_sales + $total_sales_traetments + $total_pays);
        $total_egresos = $total_comisiones + $total_expenses;
        $caja = $total - $total_egresos;
        $final->subDay()->format('Y-m-d');
        return view('box.cuadre',compact('sales','sale_traetments','pays','total_pays','total_sales_traetments','total_sales','expenses','total_expenses','total','traetments_process','inicio','final','total_comisiones','total_egresos','ventas','inicio','final','caja'));
    }

    public function list_cuadre(){
        $listCuadre = Cuadre::all();
        return view('box.listCuadre',compact('listCuadre'));
    }

    public function show_box($id){
        $cierre = Cuadre::findOrfail($id);
        $inicio = Carbon::create($cierre->start);
        $final = Carbon::create($cierre->end);

        $sales = Sale::whereBetween('created_at', [$inicio, $final]  )->get();
        $total_sales = $sales->sum('total');
        
        $sale_traetments = SaleTraetment::whereBetween('created_at',[$inicio, $final])->get();
        $total_sales_traetments = $sale_traetments->sum('payment');
        
        $expenses = Expense::whereBetween('created_at',[$inicio, $final])->get();
        $total_expenses = $expenses->sum('quantity');
        
        $pays = Payment::whereBetween('created_at',[$inicio, $final])->get();
        $total_pays = $pays->sum('payment');
        
        $traetments_process = TraetmentProcess::where('state',1)->get();
        
        $commissions = Commission::whereBetween('updated_at',[$inicio, $final])->where('is_payed',1)->get();
        
        $total_comisiones = $commissions->sum('quantity');
        

        $ventas = Sale::all();
        
        $total = ($total_sales + $total_sales_traetments + $total_pays);
        $total_egresos = $total_comisiones + $total_expenses;
        $caja = $total - $total_egresos;
        $final->subDay()->format('Y-m-d');
        return view('box.show_box',compact('sales','sale_traetments','pays','total_pays','total_sales_traetments','total_sales','expenses','total_expenses','total','traetments_process','inicio','final','total_comisiones','total_egresos','ventas','inicio','final','caja'));

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $inicio = $request->inicio;
        $inicio = Carbon::create($inicio)->format('Y-m-d');
        $final = $request->final;
        $final = Carbon::create($final)->format('Y-m-d');

        $cuadre = new Cuadre();
        $cuadre->start = $inicio;
        $cuadre->end = $final;
        $cuadre->save();

        return redirect('listCuadre');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cuadre = Cuadre::findOrFail($id);
        $cuadre->reviewed = 1;
        $cuadre->update();

        return redirect('listCuadre');
    }
}
