<?php

namespace App\Http\Controllers;

use App\Models\QuantitySession;
use Illuminate\Http\Request;

class QuantitySessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $quantities = QuantitySession::all();
        return view('quantity_session.index',compact('quantities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $quantity = QuantitySession::create($request->all());
        return redirect('quantity_session');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuantitySession  $quantitySession
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $quantity = QuantitySession::findOrFail($id);
        $quantity->update($request->all());
        return redirect('quantity_session');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuantitySession  $quantitySession
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $quantity = QuantitySession::findOrFail($id);
        $quantity->delete();

        return redirect('quantity_session');
    }
}
