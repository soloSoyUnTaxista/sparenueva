<?php

namespace App\Http\Controllers;

use App\Models\Purchase;
use App\Models\DetailPurchase;
use App\Models\Provider;
use App\Models\Product;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $purchases = Purchase::all();
        return view('purchase.index',compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $providers = Provider::all();
        $products = Product::all();
        return view('purchase.crear',compact('providers','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //metodo para Realizar una Venta
        $provider = $request->get('provider_id');
        $user = $request->get('user_id');
        $total = $request->get('total');
        $description = $request->get('description');
        $num_document = Purchase::select('num_document')->max('num_document');
        $tax = ($total * 0.12);

        try {
            //code...
            $purchase = new Purchase();
            $purchase->num_document = $num_document + 1;
            $purchase->total = $total;
            $purchase->tax = $tax;
            $purchase->description = $description;
            $purchase->provider_id = $provider;
            $purchase->user_id = $user;
            $purchase->save();
    
            $product_id = $request->get('product_id');
            $quantity = $request->get('quantity');
            $price_purchase = $request->get('price_purchase');
            $subTotal = $request->get('subTotal');
    
            $cont = 0;
    
            while($cont < count($product_id)){
                $product = Product::findOrFaiL($product_id[$cont]);
                $stock = $product->stock;
                $product->stock = $stock + $quantity[$cont];
                $product->update();
                
                $detail = new DetailPurchase();
                $detail->quantity = $quantity[$cont];
                $detail->price_purchase = $price_purchase[$cont];
                $detail->sub_total = $subTotal[$cont];
                $detail->product_id = $product_id[$cont];
                $detail->purchase_id = $purchase->id;
                $detail->save();
    
                $cont = $cont + 1;
            }
            return redirect('purchase');
        } catch (\Throwable $th) {
            throw $th;
            return redirect()->route('purchase.create');
            echo '<script>alert("No se Realizó la Venta, Compruebe que lleno todos los datos")</script>';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $purchase = Purchase::findOrFail($id);

        $details = DetailPurchase::where('purchase_id',$id)->get();
        return view('purchase.show',compact('purchase','details'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $purchase = Purchase::findOrFail($id);
        $cont = 0;

        $detail = DetailPurchase::where('purchase_id',$id)->get();
        while($cont < count($detail)){
            $quantity = $detail[$cont]->quantity;
            $idProduct = $detail[$cont]->product_id;
            $product = Product::findOrfail($idProduct);
            $stock = $product->stock;
            $product->stock = ($stock - $quantity);
            $product->update();
            $detail[$cont]->delete();

            $cont = $cont + 1;
        }
        $purchase->delete();
        return redirect('purchase');

    }
}
