<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\SaleTraetment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $sale_id = $request->get('sale_traetment_id');
        $balance = $request->get('balance');
        $payment = $request->get('payment');
        $new_balance = $request->get('newBalance');

        $sale_traetment = SaleTraetment::findOrFail($sale_id);
        $sale_traetment->balance = $new_balance;
        $sale_traetment->update();

        $pay = new Payment();
        $pay->previous_balance = $balance;
        $pay->balance = $new_balance;
        $pay->payment = $payment;
        $pay->sale_traetment_id = $sale_id;
        $pay->save();

        return redirect('sale_traetment');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
