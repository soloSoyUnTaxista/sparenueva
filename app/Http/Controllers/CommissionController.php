<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\Operator;
use App\Models\Sale;
use App\Models\TraetmentProcess;

use Illuminate\Http\Request;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $operators = Operator::all();
        $commissions = TraetmentProcess::where('state',1)->get();
        $comision = Commission::all();
        $total = 0;
        

        
        return view('comision.index',compact('operators','commissions','total'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function show(Commission $commission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $traetment_process = TraetmentProcess::where('operator_id',$id)->get();
        $sales = Sale::where('operator_id',$id)->get();

        $total = 0;
        $totalSales = 0;
        foreach($traetment_process as $tp){
            if($tp->commission->is_payed == 0){
                
                $total = ($total + $tp->commission->quantity);
            }
        }

        foreach($sales as $sale){
            if($sale->commission->is_payed == 0){
                $totalSales = $totalSales + $sale->commission->quantity;
            }
        }
        $operator = Operator::findOrFail($id);
        
        return view('comision.comision',compact('operator','traetment_process','total','totalSales','sales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $commissions = TraetmentProcess::where('operator_id',$id)->get();
        foreach ($commissions as $commission){
            $idComision = $commission->commission_id;
            $comision = Commission::find($idComision);
            $comision->is_payed = 1;
            $comision->update();
        }
        $commissions_sale = Sale::where('operator_id',$id)->get();
        foreach($commissions_sale as $comi){
            $idComi = $comi->commission_id;
            $commission = Commission::findOrFail($idComi);
            $commission->is_payed = 1;
            $commission->update();
        }
        return redirect('comision');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commission $commission)
    {
        //
    }
}
