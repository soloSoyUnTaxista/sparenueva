<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPurchase extends Model
{
    use HasFactory;

    protected $fillable = ['quantity','price_sale','sub_total','purchase_id','product_id'];
    public function purchase(){
        return $this->belongsTo('App\Models\Purchase');
    }
    public function product(){
        return $this->belongsTo('App\Models\Product');
    }
}
