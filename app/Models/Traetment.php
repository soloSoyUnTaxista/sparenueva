<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Traetment extends Model
{
    use HasFactory;

    protected $fillable = ['name','price_traetment','description','quantity_session_id','type_traetment_id'];

    public function quantity_session(){
        return $this->belongsTo('App\Models\QuantitySession');
    }

    public function type_traetment(){
        return $this->belongsTo('App\Models\TypeTraetment');
    }

    public function traetment_processes(){
        return $this->belongsTo('App\Models\TraetmentProcess');
    }
}
