<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    use HasFactory;

    protected $fillable = ['quantity','traetment_process_id'];

    public function traetment_process(){
        return $this->belongsTo('App\Models\TraetmentProcess');
    }

    public function sale(){
        return $this->belongsTo('App\Models\Sale');
    }
}
