<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = ['name','price_service','description','quantity_session_id'];

    public function quantity_sessions(){
        return $this->belongsTo('App\Models\QuantitySession');
    }
}
