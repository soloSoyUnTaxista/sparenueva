<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    use HasFactory;

    protected $fillable = ['fullname','address','phone','profile_picture'];

    public function traetment_process(){
        return $this->belongsTo('App\Models\TraetmentProcess');
    }

    public function sale_traetment(){
        return $this->belongsTo('App\Models\SaleTraetment');
    }
}
