<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name','stock','price_purchase','price_sale','description','picture_product'];

    public function detail_sales(){
        return $this->hasMany('App\Models\DetailSale');
    }

    public function detail_purchases(){
        return $this->hasMany('App\Models\DetailPurchase');
    }
}
