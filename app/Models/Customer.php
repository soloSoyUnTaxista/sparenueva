<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = ['fullname','address','phone'];

    public function sale(){
        return $this->belongsTo('App\Models\Sale');
    }

    public function sale_traetments(){
        return $this->belongsTo('App\Models\SaleTraetment');
    }

    public function traetment_processes(){
        return $this->hasMany('App\Models\TraetmentProcess');
    }
}
