<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;

    protected $fillable = ['num_document','total','tax','description','provider_id','user_id'];

    public function provider(){
        return $this->belongsTo('App\Models\Provider');
    }
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function detail_purchases(){
        return $this->hasMany('App\Models\DetailPurchase');
    }
}
