<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;

    protected $fillable = ['fullname','address','phone','bussiness_id'];

    public function bussiness(){
        return $this->belongsTo('App\Models\Bussiness');
    }
}
