<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['sale_traetment_id','previous_balance','balance','payment'];

    public function sale_traetment(){
        return $this->belongsTo('App\Models\SaleTraetment');
    }
}
