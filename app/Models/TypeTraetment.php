<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeTraetment extends Model
{
    use HasFactory;
    protected $fillable = ['name','description','porcentaje'];

    public function traetments(){
        return $this->hasMany('App\Models\Traetment');
    }
}
