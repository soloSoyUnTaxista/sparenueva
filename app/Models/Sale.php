<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $fillable = ['num_document','total','description','customer_id','user_id','operator_id'];

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }

    public function detail_sales(){
        return $this->hasMany('App\Models\DetailSale');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function commission(){
        return $this->belongsTo('App\Models\Commission');
    }
}
