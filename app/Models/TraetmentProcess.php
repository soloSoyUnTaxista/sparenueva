<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraetmentProcess extends Model
{
    use HasFactory;

    protected $fillable = ['num_session','price','sale_traetment_id','traetment_id','operator_id'];

    public function operator(){
        return $this->belongsTo('App\Models\Operator');
    }

    public function traetment(){
        return $this->belongsTo('App\Models\Traetment');
    }

    public function commission(){
        return $this->belongsTo('App\Models\Commission');
    }

    public function sale_traetment(){
        return $this->belongsTo('App\Models\SaleTraetment');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }
}
