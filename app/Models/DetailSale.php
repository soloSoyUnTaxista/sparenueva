<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailSale extends Model
{
    use HasFactory;

    protected $fillable = ['quantity','price_sale','sub_total','discount','sales_id','product_id'];

    public function sale(){
        return $this->belongsTo('App\Models\Sale');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }
}
