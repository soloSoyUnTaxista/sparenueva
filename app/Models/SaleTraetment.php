<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleTraetment extends Model
{
    use HasFactory;

    protected $fillable=['num_document','total','payment','balance','description','customer_id','operator_id','totalDiscount'];

    public function payments(){
        return $this->hasMany('App\Models\Payment');
    }
    public function detail_sale_traetments(){
        return $this->hasMany('App\Models\DetailSaleTraetment');
    }
    public function traetment_processes(){
        return $this->hasMany('App\Models\TraetmentProcess');
    }

    public function customer(){
        return $this->belongsTo('App\Models\Customer');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    public function operator(){
        return $this->belongsTo('App\Models\Operator');
    }
}
