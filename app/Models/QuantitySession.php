<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuantitySession extends Model
{
    use HasFactory;
    protected $fillable = ['quantity_session','time_frame','description'];

    public function traetments(){
        return $this->hasMany('App\Models\Traetment');
    }
}
