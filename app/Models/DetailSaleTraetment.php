<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailSaleTraetment extends Model
{
    use HasFactory;
    protected $fillable = ['quantity','price_traetment','sub_total','sale_traetment_id','traetment_id','discount'];

    public function sale_traetment(){
        return $this->belongsTo('App\Models\SaleTraetment');
    }
    public function traetment(){
        return $this->belongsTo('App\Models\Traetment');
    }
}
